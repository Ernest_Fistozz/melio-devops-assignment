FROM clojure:openjdk-8-lein-2.9.5
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app/infra-problem
RUN apt update && \
    apt install -y make python3 && \
    apt clean
COPY . ./
RUN make libs && \
    make clean all